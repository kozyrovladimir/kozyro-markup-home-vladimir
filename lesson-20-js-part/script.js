var yourPatronymic = prompt('Введите вашу фамилию:', '');
var yourFirstName = prompt('Введите ваше имя:', '');
var yourSurname = prompt('Введите ваше отчество:', '');
var yourAge = prompt('Ваш возраст:', '');
var yourGenderMan = confirm('Ваш пол мужской?');
var yourAgeInDays = +yourAge * 365;
var yourAgeAfterFiveYears = +yourAge + 5;
var answerToTheQuestion;
var yourGender;

if (yourGenderMan == true) {
    yourGender = 'мужской';
} else {
    yourGender = 'женский';
}

if (yourGenderMan == true) {
    if (yourAge > 63) {
         answerToTheQuestion = 'да';
    } else {
         answerToTheQuestion = 'нет';
    }
} else {
    answerToTheQuestion = (yourAge > 58) ? 'да' : 'нет';
}

alert('Ваше ФИО:' + ' ' + yourPatronymic + ' ' + yourFirstName + ' ' + yourSurname + '\n' + 'Ваш возраст в годах:' + ' ' + yourAge + '\n' + 'Ваш возраст в днях:' + ' ' + yourAgeInDays + '\n' + 'Через 5 лет вам будет:' + ' ' + yourAgeAfterFiveYears + '\n' + 'Ваш пол:' + ' ' + yourGender + '\n' + 'Вы на пенсии:' + ' ' + answerToTheQuestion);
